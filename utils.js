const escape = (str) => {
    str = str.replace( /</g, '&lt;' )
    str = str.replace( />/g, '&gt;' )
    str = str.replace( /&/g, '&amp;' )
    str = str.replace( /"/g, '&quot;' )
    return str
}

module.exports = {
    deleteMessage: (msg) => {
        return bot.telegram.deleteMessage(msg.chat.id, msg.message_id)
    },
    isAdmin: async (chat, user) => {
        try {
            var status = await bot.telegram.getChatMember(chat, user)
            if ( ! status ) return false
    
            if ( status.status == 'creator' || status.status == 'administrator' )
                return true
            else
                return false
        } catch(e) {
            return false
        }
    },
    mention: (user) => {
        return `<a href="tg://user?id=${user.id}">${escape(user.first_name)}</a>`
    },
}